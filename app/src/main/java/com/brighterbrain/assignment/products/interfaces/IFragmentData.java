package com.brighterbrain.assignment.products.interfaces;


import com.brighterbrain.assignment.products.models.Product;

public interface IFragmentData {
    public void updateData(Product product);
}
