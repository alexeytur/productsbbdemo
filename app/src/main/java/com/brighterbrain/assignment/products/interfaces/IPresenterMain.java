package com.brighterbrain.assignment.products.interfaces;


public interface IPresenterMain {
    void onCreateProductClicked();
    void onShowProductsClicked();
}
