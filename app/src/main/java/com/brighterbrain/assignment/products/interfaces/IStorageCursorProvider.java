package com.brighterbrain.assignment.products.interfaces;

import android.database.Cursor;

import com.brighterbrain.assignment.products.models.Product;


public interface IStorageCursorProvider {
    Cursor getCursor();
    void closeCursor();
    Product cursorToProduct(Cursor cursor);
}
