package com.brighterbrain.assignment.products.views.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.brighterbrain.assignment.products.R;
import com.brighterbrain.assignment.products.storage.ProductDbContract;


public class ProductCursorAdapter extends CursorAdapter {

    public ProductCursorAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.item_product, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // Find fields to populate in inflated template
        TextView title = (TextView) view.findViewById(R.id.item_title);
        TextView price = (TextView) view.findViewById(R.id.item_price);
        // Get product data from cursor
        String name = cursor.getString(cursor.getColumnIndexOrThrow(ProductDbContract.ProductDb.NAME));
        String itemPrice = cursor.getString(cursor.getColumnIndexOrThrow(ProductDbContract.ProductDb.PRICE_REGULAR));
        title.setText(name);
        price.setText(context.getString(R.string.currency_symbol) + itemPrice);
    }
}
