package com.brighterbrain.assignment.products;

import android.app.Application;
import android.content.Context;

/**
 * Application will be instantiated first.
 * So use it as static provider of app context
 */

public class App extends Application {
    private static Context context;
    private static MockProductManager productManager;

    public static Context getContext() {
        return context;
    }

    public static MockProductManager getMockProductManager() {
        return productManager;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        productManager = new MockProductManager(context);
    }
}
