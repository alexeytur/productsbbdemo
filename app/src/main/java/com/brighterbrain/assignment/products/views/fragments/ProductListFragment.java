package com.brighterbrain.assignment.products.views.fragments;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.brighterbrain.assignment.products.R;
import com.brighterbrain.assignment.products.interfaces.IFragmentData;
import com.brighterbrain.assignment.products.interfaces.IStorageCursorProvider;
import com.brighterbrain.assignment.products.models.Product;
import com.brighterbrain.assignment.products.storage.ProductDbImpl;
import com.brighterbrain.assignment.products.views.adapters.ProductCursorAdapter;

public class ProductListFragment extends Fragment implements ListView.OnItemClickListener, IFragmentData {

    private final String PRODUCTS_GONE_VISIBLE = "prod_gone_visible";
    public interface OnItemSelectedListener {
        public void onItemSelected(Product product);
    }

    private OnItemSelectedListener callback;
    private ListView listView;
    private TextView tvNoItems;
    private ProductCursorAdapter adapter;
    private IStorageCursorProvider storage;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_list, container, false);
        listView = (ListView) rootView.findViewById(R.id.list);
        tvNoItems = (TextView) rootView.findViewById(R.id.list_no_items);

        if (storage == null)
            storage = new ProductDbImpl(getContext());

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                adapter = new ProductCursorAdapter(getContext(), storage.getCursor());
                listView.setAdapter(adapter);
                setNoItemVisibility();
            }

        });

        listView.setOnItemClickListener(this);
        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(PRODUCTS_GONE_VISIBLE, tvNoItems.getVisibility());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState != null && tvNoItems != null) {
            int vis = savedInstanceState.getInt(PRODUCTS_GONE_VISIBLE);
            tvNoItems.setVisibility(vis == 0 ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callback = (OnItemSelectedListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        storage.closeCursor();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Cursor cursor = (Cursor) listView.getItemAtPosition(position);
        Product product = storage.cursorToProduct(cursor);
        callback.onItemSelected(product);
    }

    @Override
    public void updateData(Product product) {
        if (adapter != null) {
            storage.closeCursor();
            adapter.swapCursor(storage.getCursor());
            adapter.notifyDataSetChanged();
        }
    }

    private void setNoItemVisibility() {
        if (adapter != null && tvNoItems != null) {
            if (adapter.getCount() > 0)
                tvNoItems.setVisibility(View.GONE);
            else
                tvNoItems.setVisibility(View.VISIBLE);
        }
    }


}
