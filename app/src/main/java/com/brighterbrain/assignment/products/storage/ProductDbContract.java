package com.brighterbrain.assignment.products.storage;


import android.provider.BaseColumns;

public final class ProductDbContract {
    // we don't need object out of this class, so private CTOR
    private ProductDbContract() {}

    public static class ProductDb implements BaseColumns {
        public static final String TABLE_NAME = "products";

        public static final String PRODUCT_ID = "product_id";
        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String PRICE_REGULAR = "priceRegular";
        public static final String PRICE_SALE = "priceSale";
        public static final String URL_PHOTO = "urlPhoto";
        public static final String COLORS = "colors";
        public static final String STORES = "stores";
    }
}
