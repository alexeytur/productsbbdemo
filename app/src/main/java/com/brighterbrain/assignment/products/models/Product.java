package com.brighterbrain.assignment.products.models;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

/**
 * Model class representing product
 */

public class Product implements Serializable{

    private int id;
    private String name;
    private String description;
    private float priceRegular;
    private float priceSale;
    private String urlPhoto;
    private List<String> colors;
    private Map<String, Integer> stores;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public float getPriceRegular() {
        return priceRegular;
    }

    public float getPriceSale() {
        return priceSale;
    }

    public String getUrlPhoto() {
        return urlPhoto;
    }

    public List<String> getColors() {
        return colors;
    }

    public Map<String, Integer> getStores() {
        return stores;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPriceRegular(float priceRegular) {
        this.priceRegular = priceRegular;
    }

    public void setPriceSale(float priceSale) {
        this.priceSale = priceSale;
    }

    public void setUrlPhoto(String urlPhoto) {
        this.urlPhoto = urlPhoto;
    }

    public void setColors(List<String> colors) {
        this.colors = colors;
    }

    public void setStores(Map<String, Integer> stores) {
        this.stores = stores;
    }

    //
    // Use GSON lib to convert list and map to string to store it in SQLite
    //

    public String getColorsAsString() {
        Gson gson = new Gson();
        return gson.toJson(colors);
    }

    public void setColorsFromString(String string) {
        Gson gson = new Gson();
        Type type= new TypeToken<List<String>>(){}.getType();
        colors = gson.fromJson(string, type);
    }

    public String getStoresAsString() {
        Gson gson = new Gson();
        return gson.toJson(stores);
    }

    public void setStoresFromString(String string) {
        Gson gson = new Gson();
        Type type= new TypeToken<Map<String, Integer>>(){}.getType();
        stores = gson.fromJson(string, type);
    }


}
