package com.brighterbrain.assignment.products.views.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.AsyncTaskLoader;

import com.brighterbrain.assignment.products.interfaces.IStorageCursorProvider;
import com.brighterbrain.assignment.products.storage.ProductDbImpl;

/**
 * Use this custom loader to load cursor from DB
 */

public class ListCursorLoader extends AsyncTaskLoader<Cursor> {

    private IStorageCursorProvider storage;

    public ListCursorLoader(Context context) {
        super(context);
        storage = new ProductDbImpl(context);
    }

    @Override
    public Cursor loadInBackground() {
        return storage.getCursor();
    }
}
