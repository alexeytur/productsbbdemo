package com.brighterbrain.assignment.products.views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.brighterbrain.assignment.products.R;
import com.brighterbrain.assignment.products.interfaces.IPresenterMain;
import com.brighterbrain.assignment.products.presenters.PresenterMain;
import com.brighterbrain.assignment.products.interfaces.IViewMain;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, IViewMain {

    IPresenterMain presenterMain;
    Button btnProductCreate;
    Button btnProductsShow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //create presenter and pass IViewMain for callbacks routine
        presenterMain = new PresenterMain(this);

        btnProductCreate = (Button)findViewById(R.id.product_create);
        btnProductsShow = (Button)findViewById(R.id.product_show);

        btnProductCreate.setOnClickListener(this);
        btnProductsShow.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.product_create:
                presenterMain.onCreateProductClicked();
                break;
            case R.id.product_show:
                presenterMain.onShowProductsClicked();
                break;
            default:
                break;
        }
    }

    @Override
    public void showProductCreated(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProductError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void navigateToListOfProducts() {
        startActivity(new Intent(this, ProductListActivity.class));
    }
}
