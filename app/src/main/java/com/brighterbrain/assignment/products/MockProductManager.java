package com.brighterbrain.assignment.products;

import android.content.Context;
import android.util.Log;

import com.brighterbrain.assignment.products.models.Product;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * MockProductManager is a class that loads products from JSON and deserialize them
 * into into POJO Product objects
 */

public class MockProductManager {
    private List<Product> list;
    private Context context;

    public MockProductManager(Context context) {
        // Its predefined that we have 5 products
        // so just initialize them here
        this.context = context;
        list = new ArrayList<Product>();
        loadJSONFromAsset();
        Log.i("TAG", "List = " + list.toString());
    }

    public List<Product> getMockProductList() {
        return list;
    }

    private void loadJSONFromAsset() {
        String str = null;

        InputStream inputStream = null;
        try {
            inputStream = context.getAssets().open("products.json");
            byte[] buf = new byte[inputStream.available()];
            inputStream.read(buf);
            inputStream.close();
            str = new String(buf, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (str == null)
            return;

        Gson gson = new Gson();
        list = gson.fromJson(str, new TypeToken<ArrayList<Product>>() {}.getType());
    }
}
