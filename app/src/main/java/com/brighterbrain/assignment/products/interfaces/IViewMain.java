package com.brighterbrain.assignment.products.interfaces;


public interface IViewMain {
    void showProductCreated(String message);
    void showProductError(String error);
    void navigateToListOfProducts();
}
