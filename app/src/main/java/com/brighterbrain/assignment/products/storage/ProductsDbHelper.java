package com.brighterbrain.assignment.products.storage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class ProductsDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "products.db";

    private static final String SQL_CREATE_PRODUCTS =
            "CREATE TABLE " + ProductDbContract.ProductDb.TABLE_NAME + " (" +
                    ProductDbContract.ProductDb._ID + " INTEGER PRIMARY KEY," +
                    ProductDbContract.ProductDb.PRODUCT_ID + " INTEGER," +
                    ProductDbContract.ProductDb.NAME + " TEXT," +
                    ProductDbContract.ProductDb.DESCRIPTION + " TEXT," +
                    ProductDbContract.ProductDb.PRICE_REGULAR + "  REAL," +
                    ProductDbContract.ProductDb.PRICE_SALE + " REAL," +
                    ProductDbContract.ProductDb.URL_PHOTO + " TEXT," +
                    ProductDbContract.ProductDb.COLORS + " TEXT," +
                    ProductDbContract.ProductDb.STORES + " TEXT)";

    private static final String SQL_DELETE_PRODUCTS =
            "DROP TABLE IF EXISTS " + ProductDbContract.ProductDb.TABLE_NAME;

    public ProductsDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_PRODUCTS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
