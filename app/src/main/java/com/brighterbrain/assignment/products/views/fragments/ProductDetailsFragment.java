package com.brighterbrain.assignment.products.views.fragments;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.brighterbrain.assignment.products.App;
import com.brighterbrain.assignment.products.R;
import com.brighterbrain.assignment.products.interfaces.IFragmentData;
import com.brighterbrain.assignment.products.interfaces.IStorage;
import com.brighterbrain.assignment.products.interfaces.IStorageCursorProvider;
import com.brighterbrain.assignment.products.models.Product;
import com.brighterbrain.assignment.products.storage.ProductDbImpl;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class ProductDetailsFragment extends Fragment implements
        ProductListFragment.OnItemSelectedListener,
        IFragmentData {

    public interface OnItemModifyListener {
        public void onItemDelete(Product product);
        public void onItemUpdate(Product product);
    }

    public static final String PRODUCT_DETAIL = "prod_detail";
    private Product product;

    private Drawable productImage;
    private final int shortAnimationDuration = 500;

    private TextView title;
    private TextView descip;
    private TextView priceReg;
    private TextView priceSale;
    private TextView colors;
    private TextView stores;
    private ImageButton imageButton;
    private Button btnUpdate;
    private Button btnDelete;

    private Animator currentAnimator;

    private OnItemModifyListener callback;
    private IStorage storage;

    private void setValuesToView(Product product) {
        title.setText(product.getName());
        descip.setText(product.getDescription());
        priceReg.setText(getString(R.string.start_price_reg) + Float.toString(product.getPriceRegular()));
        priceSale.setText(getString(R.string.start_price_sale) + Float.toString(product.getPriceSale()));

        StringBuilder bld = new StringBuilder();
        List<String> list = product.getColors();
        bld.append(getString(R.string.start_colors));
        for (String str : list) {
            bld.append(str).append(" ");
        }
        colors.setText(bld.toString());

        bld.setLength(0);
        bld.append(getString(R.string.start_stores));
        Map<String, Integer> map = product.getStores();
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            bld.append(entry.getKey()).append(": ").append(entry.getValue()).append(" ");
        }
        stores.setText(bld.toString());
        bld.setLength(0);

        productImage = null;
        try {
            productImage = Drawable.createFromStream(getContext().getAssets().open(product.getUrlPhoto()), null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        imageButton.setImageDrawable(productImage);
        imageButton.setScaleType(ImageView.ScaleType.FIT_START);
        imageButton.setAdjustViewBounds(true);
        imageButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zoomImage(v);
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productDataUpdate(v);
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productDataDelete(v);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.item_product_details, container, false);
        if (storage == null)
            storage = new ProductDbImpl(getContext());

        title = (TextView) rootView.findViewById(R.id.detailTitle);
        descip = (TextView) rootView.findViewById(R.id.detailDescrip);
        priceReg = (TextView) rootView.findViewById(R.id.detailPriceReg);
        priceSale = (TextView) rootView.findViewById(R.id.detailPriceSale);
        colors = (TextView) rootView.findViewById(R.id.detailColors);
        stores = (TextView) rootView.findViewById(R.id.detailStores);
        imageButton = (ImageButton) rootView.findViewById(R.id.detailImageButton);
        btnUpdate = (Button) rootView.findViewById(R.id.detailBtnUpdate);
        btnDelete = (Button) rootView.findViewById(R.id.detailBtnDelete);

        if(savedInstanceState != null)
            product = (Product)savedInstanceState.getSerializable(PRODUCT_DETAIL);

        if (product != null) {
            setValuesToView(product);
        }

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callback = (OnItemModifyListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ((ProductDbImpl)storage).closeCursor();
    }

    void productDataUpdate(View v) {
        List<Product> list = App.getMockProductManager().getMockProductList();
        for (Product mockProduct : list) {
            if (product.getId() == mockProduct.getId()) {
                product = mockProduct;
                break;
            }
        }
        setValuesToView(product);
        getView().invalidate();
        callback.onItemUpdate(product);
    }

    void productDataDelete(View v) {
        storage.deleteProduct(product);
        callback.onItemDelete(product);
    }

    @Override
    public void onItemSelected(Product product) {

    }

    @Override
    public void updateData(Product product) {
        this.product = product;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(PRODUCT_DETAIL, product);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState != null)
            product = (Product) savedInstanceState.getSerializable(PRODUCT_DETAIL);
    }

    // code was copied from android animation tutorial
    // https://developer.android.com/training/animation/zoom.html
    private void zoomImage(View thumbView) {
        final View v = thumbView;
        // If there's an animation in progress, cancel it
        // immediately and proceed with this one.
        if (currentAnimator != null) {
            currentAnimator.cancel();
        }

        // Load the high-resolution "zoomed-in" imageButton.
        final ImageView expandedImageView = (ImageView) getView().findViewById(
                R.id.expanded_image);
        expandedImageView.setImageDrawable(productImage);

        // Calculate the starting and ending bounds for the zoomed-in imageButton.
        // This step involves lots of math. Yay, math.
        final Rect startBounds = new Rect();
        final Rect finalBounds = new Rect();
        final Point globalOffset = new Point();

        // The start bounds are the global visible rectangle of the thumbnail,
        // and the final bounds are the global visible rectangle of the container
        // view. Also set the container view's offset as the origin for the
        // bounds, since that's the origin for the positioning animation
        // properties (X, Y).
        v.getGlobalVisibleRect(startBounds);
        getView().findViewById(R.id.container)
                .getGlobalVisibleRect(finalBounds, globalOffset);
        startBounds.offset(-globalOffset.x, -globalOffset.y);
        finalBounds.offset(-globalOffset.x, -globalOffset.y);

        // Adjust the start bounds to be the same aspect ratio as the final
        // bounds using the "center crop" technique. This prevents undesirable
        // stretching during the animation. Also calculate the start scaling
        // factor (the end scaling factor is always 1.0).
        float startScale;
        if ((float) finalBounds.width() / finalBounds.height()
                > (float) startBounds.width() / startBounds.height()) {
            // Extend start bounds horizontally
            startScale = (float) startBounds.height() / finalBounds.height();
            float startWidth = startScale * finalBounds.width();
            float deltaWidth = (startWidth - startBounds.width()) / 2;
            startBounds.left -= deltaWidth;
            startBounds.right += deltaWidth;
        } else {
            // Extend start bounds vertically
            startScale = (float) startBounds.width() / finalBounds.width();
            float startHeight = startScale * finalBounds.height();
            float deltaHeight = (startHeight - startBounds.height()) / 2;
            startBounds.top -= deltaHeight;
            startBounds.bottom += deltaHeight;
        }

        // Hide the thumbnail and show the zoomed-in view. When the animation
        // begins, it will position the zoomed-in view in the place of the
        // thumbnail.
        v.setAlpha(0f);
        expandedImageView.setVisibility(View.VISIBLE);

        // Set the pivot point for SCALE_X and SCALE_Y transformations
        // to the top-left corner of the zoomed-in view (the default
        // is the center of the view).
        expandedImageView.setPivotX(0f);
        expandedImageView.setPivotY(0f);

        // Construct and run the parallel animation of the four translation and
        // scale properties (X, Y, SCALE_X, and SCALE_Y).
        AnimatorSet set = new AnimatorSet();
        set
                .play(ObjectAnimator.ofFloat(expandedImageView, View.X,
                        startBounds.left, finalBounds.left))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
                        startBounds.top, finalBounds.top))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
                        startScale, 1f)).with(ObjectAnimator.ofFloat(expandedImageView,
                View.SCALE_Y, startScale, 1f));
        set.setDuration(shortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                currentAnimator = null;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                currentAnimator = null;
            }
        });
        set.start();
        currentAnimator = set;

        // Upon clicking the zoomed-in imageButton, it should zoom back down
        // to the original bounds and show the thumbnail instead of
        // the expanded imageButton.
        final float startScaleFinal = startScale;
        expandedImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (currentAnimator != null) {
                    currentAnimator.cancel();
                }

                // Animate the four positioning/sizing properties in parallel,
                // back to their original values.
                AnimatorSet set = new AnimatorSet();
                set.play(ObjectAnimator
                        .ofFloat(expandedImageView, View.X, startBounds.left))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.Y,startBounds.top))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.SCALE_X, startScaleFinal))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.SCALE_Y, startScaleFinal));
                set.setDuration(shortAnimationDuration);
                set.setInterpolator(new DecelerateInterpolator());
                set.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setAlpha(1f);
                        v.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        currentAnimator = null;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        view.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        currentAnimator = null;
                    }
                });
                set.start();
                currentAnimator = set;
            }
        });
    }

}
