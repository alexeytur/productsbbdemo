package com.brighterbrain.assignment.products.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.brighterbrain.assignment.products.interfaces.IStorage;
import com.brighterbrain.assignment.products.interfaces.IStorageCursorProvider;
import com.brighterbrain.assignment.products.models.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Actual implementation of Product database
 */

public class ProductDbImpl implements IStorage, IStorageCursorProvider {

    private ProductsDbHelper dbHelper = null;
    private String[] projection = {
            ProductDbContract.ProductDb._ID,
            ProductDbContract.ProductDb.PRODUCT_ID,
            ProductDbContract.ProductDb.NAME,
            ProductDbContract.ProductDb.DESCRIPTION,
            ProductDbContract.ProductDb.PRICE_REGULAR,
            ProductDbContract.ProductDb.PRICE_SALE,
            ProductDbContract.ProductDb.URL_PHOTO,
            ProductDbContract.ProductDb.COLORS,
            ProductDbContract.ProductDb.STORES
    };
    private Cursor cursorToShare;

    public ProductDbImpl (Context context) {
        dbHelper = new ProductsDbHelper(context);
    }

    @Override
    public Cursor getCursor () {
        if (cursorToShare != null && cursorToShare.isClosed() == false)
            return cursorToShare;
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        cursorToShare = db.query(ProductDbContract.ProductDb.TABLE_NAME,
                projection, null, null, null, null, null);

        cursorToShare.moveToFirst();
        return cursorToShare;
    }

    @Override
    public void closeCursor() {
        if (cursorToShare != null && cursorToShare.isClosed() == false)
            cursorToShare.close();
    }

    @Override
    public int createProduct(Product product) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ProductDbContract.ProductDb.PRODUCT_ID, product.getId());
        values.put(ProductDbContract.ProductDb.NAME, product.getName());
        values.put(ProductDbContract.ProductDb.DESCRIPTION, product.getDescription());
        values.put(ProductDbContract.ProductDb.PRICE_REGULAR, product.getPriceRegular());
        values.put(ProductDbContract.ProductDb.PRICE_SALE, product.getPriceSale());
        values.put(ProductDbContract.ProductDb.URL_PHOTO, product.getUrlPhoto());
        values.put(ProductDbContract.ProductDb.COLORS, product.getColorsAsString());
        values.put(ProductDbContract.ProductDb.STORES, product.getStoresAsString());
        return (int) db.insert(ProductDbContract.ProductDb.TABLE_NAME, null, values);
    }

    @Override
    public int updateProduct(Product product) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ProductDbContract.ProductDb.PRODUCT_ID, product.getId());
        values.put(ProductDbContract.ProductDb.NAME, product.getName());
        values.put(ProductDbContract.ProductDb.DESCRIPTION, product.getDescription());
        values.put(ProductDbContract.ProductDb.PRICE_REGULAR, product.getPriceRegular());
        values.put(ProductDbContract.ProductDb.PRICE_SALE, product.getPriceSale());
        values.put(ProductDbContract.ProductDb.URL_PHOTO, product.getUrlPhoto());
        values.put(ProductDbContract.ProductDb.COLORS, product.getColorsAsString());
        values.put(ProductDbContract.ProductDb.STORES, product.getStoresAsString());
        return db.update(ProductDbContract.ProductDb.TABLE_NAME,
                values,
                ProductDbContract.ProductDb.PRODUCT_ID + " = '" + product.getId() + "'",
                null);
    }

    @Override
    public int deleteProduct(Product product) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete(ProductDbContract.ProductDb.TABLE_NAME,
                ProductDbContract.ProductDb.PRODUCT_ID + " = '" + product.getId() + "'", null);
    }

    @Override
    public List<Product> getAllProducts() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        List<Product> res = new ArrayList<Product>();
        Cursor cursor = db.query(ProductDbContract.ProductDb.TABLE_NAME,
                projection, null, null, null, null, null);
        while (cursor.moveToNext()) {
            res.add(cursorToProduct(cursor));
        }
        cursor.close();
        return res;
    }

    @Override
    public Product getProductById() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String selection = ProductDbContract.ProductDb.PRODUCT_ID + " = ?";
        Cursor cursor = db.query(ProductDbContract.ProductDb.TABLE_NAME,
                projection, selection, null, null, null, null);
        cursor.close();
        return null;
    }


    // helper method to extract Product data from current entry where cursor points
    @Override
    public Product cursorToProduct(Cursor cursor) {
        Product product = new Product();
        product.setId(cursor.getInt(cursor.getColumnIndex(ProductDbContract.ProductDb.PRODUCT_ID)));
        product.setName(cursor.getString(cursor.getColumnIndex(ProductDbContract.ProductDb.NAME)));
        product.setDescription(cursor.getString(cursor.getColumnIndex(ProductDbContract.ProductDb.DESCRIPTION)));
        product.setPriceRegular(cursor.getFloat(cursor.getColumnIndex(ProductDbContract.ProductDb.PRICE_REGULAR)));
        product.setPriceSale(cursor.getFloat(cursor.getColumnIndex(ProductDbContract.ProductDb.PRICE_SALE)));
        product.setUrlPhoto(cursor.getString(cursor.getColumnIndex(ProductDbContract.ProductDb.URL_PHOTO)));
        product.setColorsFromString(cursor.getString(cursor.getColumnIndex(ProductDbContract.ProductDb.COLORS)));
        product.setStoresFromString(cursor.getString(cursor.getColumnIndex(ProductDbContract.ProductDb.STORES)));
        return product;
    }
}
