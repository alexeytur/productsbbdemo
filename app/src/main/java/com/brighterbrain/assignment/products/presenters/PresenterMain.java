package com.brighterbrain.assignment.products.presenters;

import com.brighterbrain.assignment.products.App;
import com.brighterbrain.assignment.products.interfaces.IPresenterMain;
import com.brighterbrain.assignment.products.interfaces.IStorage;
import com.brighterbrain.assignment.products.interfaces.IViewMain;
import com.brighterbrain.assignment.products.models.Product;
import com.brighterbrain.assignment.products.storage.ProductDbImpl;

import java.util.List;

/**
 * Presenter for MainActivity
 */

public class PresenterMain implements IPresenterMain{

    IViewMain viewMain;
    IStorage storage;


    public PresenterMain(IViewMain view) {
        viewMain = view;
        storage = new ProductDbImpl(App.getContext());
    }


    @Override
    public void onCreateProductClicked() {
        // get Product list from MockProductManager
        List<Product> mockProductList = App.getMockProductManager().getMockProductList();
        if (mockProductList.isEmpty()) {
            viewMain.showProductError("No products for creation");
        }
        // get Product list from db
        List<Product> dbProductList = storage.getAllProducts();

        if (mockProductList.size() == dbProductList.size()) {
            viewMain.showProductCreated("All products are created");
            return;
        }

        try {
            // here we assume that dbList could not be bigger mockList
            Product prod = mockProductList.get(dbProductList.size());
            int res = storage.createProduct(prod);
            if (res != -1) {
                viewMain.showProductCreated(prod.getName() + "is created");
            }
        }
        catch (Exception e) {
            viewMain.showProductError("Error occurred in product creation");
        }
    }

    @Override
    public void onShowProductsClicked() {
        List<Product> dbProductList = storage.getAllProducts();
        if (dbProductList.isEmpty())
            viewMain.showProductError("Please create product first");
        else
            viewMain.navigateToListOfProducts();
    }
}
