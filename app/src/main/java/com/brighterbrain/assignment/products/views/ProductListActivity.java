package com.brighterbrain.assignment.products.views;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.brighterbrain.assignment.products.R;
import com.brighterbrain.assignment.products.interfaces.IFragmentData;
import com.brighterbrain.assignment.products.models.Product;
import com.brighterbrain.assignment.products.views.fragments.ProductDetailsFragment;
import com.brighterbrain.assignment.products.views.fragments.ProductListFragment;

/**
 * Activity that holds list if created products
 */

public class ProductListActivity extends AppCompatActivity implements
        ProductListFragment.OnItemSelectedListener,
        ProductDetailsFragment.OnItemModifyListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products_list);

        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.frag_container_list) != null) {

            if (savedInstanceState != null) {
                return;
            }
            // Create a new Fragment to be placed in the activity layout
            IFragmentData listFragment = new ProductListFragment();

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            ((ProductListFragment)listFragment).setArguments(getIntent().getExtras());

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.frag_container_list, ((ProductListFragment)listFragment)).commit();
        }
    }

    @Override
    public void onItemSelected(Product product) {
        // Otherwise, only one fragment
        ProductDetailsFragment newFragment = new ProductDetailsFragment();
        newFragment.updateData(product);
        // replace container with list transaction
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(R.id.frag_container_list, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onItemDelete(Product product) {
        getSupportFragmentManager().popBackStackImmediate();
        IFragmentData listFragment = null;
        try {
            listFragment = (ProductListFragment) getSupportFragmentManager().findFragmentById(R.id.frag_container_list);
        }
        catch (Exception e) {
        }
        if (listFragment != null) {
            listFragment.updateData(product);
        }
    }

    @Override
    public void onItemUpdate(Product product) {
        Toast.makeText(this, product.getName() + getString(R.string.is_updated), Toast.LENGTH_SHORT).show();
    }
}
