package com.brighterbrain.assignment.products.interfaces;

import com.brighterbrain.assignment.products.models.Product;

import java.util.List;

public interface IStorage {
    int createProduct(Product product);
    int updateProduct(Product product);
    int deleteProduct(Product product);
    List<Product> getAllProducts();
    Product getProductById();
}
